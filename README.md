# NFS

1. Стенд поднимает две виртуалки 
```
Master 192.168.11.150
Slave 192.168.11.151
на Master шарится папка /mnt/storage на readonly и подкаталог /mnt/storage/upload на readwrite
```


2. На master и slave устанавливается `nfs-utils`
3. на master открываются порты на firewalld 
4. расшаривается директория /mnt/storage на readonly  (добавление записи в /etc/exports)             
```echo "/mnt/storage           192.168.11.151(ro,sync,no_subtree_check,fsid=2,crossmnt)" >> /etc/exports``` 

- crossmnt позволяет монтировать подпапку без наследования прав

- fsid устанавливает порядок монтирования


5. расшаривается директория /mnt/storage/upload на readwrite
```echo "/mnt/storage/upload           192.168.11.151(rw,sync,subtree_check,fsid=1)" >> /etc/exports```
6. экспортируются расшаренные папки `exportfs -r`

7. На slave создается папка куда будет примонтирова шара `/mnt/nfs-share`
8. добавляются записи в fstab
```
echo "192.168.11.150:/mnt/storage /mnt/nfs-share nfs nfsvers=3,proto=udp,hard,intr,rsize=32768,wsize=32768,noatime 0 0" >> /etc/fstab
echo "192.168.11.150:/mnt/storage/upload /mnt/nfs-share/upload nfs nfsvers=3,proto=udp,hard,intr,rsize=32768,wsize=32768,noatime 0 0" >> /etc/fstab
```
что монтировать, куда монтировать, тип файловой системы, версия nfs, используемый протокол  итд

9. перечитывается fstab `mount -a`